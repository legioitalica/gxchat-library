-- Author      : Galilman
-- Create Date : 28/10/2014 16:27:27

local lib = LibStub:NewLibrary("gxchat-LibEmoticons", 1);

if (not lib) then
	return;
end

local tabStili = {};
local tabEmoticons = {};

local function trim(s)
	return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local function literalize(str)
    return str:gsub("[%(%)%.%%%+%-%*%?%[%]%^%$]", "%%%0")
end

--- API for external addons to initialize a new style
-- @param name Style name
function lib:AddStyle (name)
	if (not name or type (name) ~= "string" or trim(name) == "") then
		error ("name must be a non-empty string");
	end
	if (tabStili[name]) then
		error ("style already registered");
	end

	tabStili[name] = name;
	tabEmoticons[name] = {};
end

--- API for external addons to add an emoticon
-- @param style Style name
-- @param code Emoticon code (for example :) :( etc.)
-- @param path path to the emoticon's image
function lib:AddEmoticon (style, code, path)
	if (not style or type (style) ~= "string" or trim(style) == "") then
		error ("style must be a non-empty string");
	end
	if (not code or type (code) ~= "string") then
		error ("code must be a string");
	end 
	if (not path or type (style) ~= "string" or trim (path) == "") then
		error ("path must be a non-empty string");
	end
	if (not tabEmoticons[style]) then
		self:AddStyle (style);
	end
	tabEmoticons[style][code] = path;
end

--- API for external addons to add emoticons
-- @param style Style name
-- @param tab Table with emoticons image path
-- @usage 
--	local tab = {
--		[":)"] = "Interface\\AddOns\\addon_name\\path_image"
--	}
--	libEmoticons:AddEmoticonsFromTable (style, tab);
function lib:AddEmoticonsFromTable (style, tab)
	if (not tab or type (tab) ~= "table") then
		error ("tab: table expected, got " .. type (tab));
	end
	for k, v in pairs (tab) do
		if (type(k) == "string" and type (v) == "string") then
			self:AddEmoticon (style, k, v);
		end
	end
end

local function addEmoticon (messaggio, style)
	local msg = messaggio;
	local tab = tabEmoticons[style];
	if (tab) then
		for k, v in pairs (tab) do
			msg = string.gsub (msg, literalize (k), "|T" .. v .. ":16|t");
		end
	end
	return msg;
end

--- API for external addons to get a message with emoticons
-- @param message	original message
-- @param style emoticons style
-- @return message with emoticons
function lib:FormatMessage (message, style)
	if (not style or type (style) ~= "string" or trim (style) == "" or not tabStili[style]) then
		return message;
	end
	local msg = "";
	local startPos = 1;
	local endPos;
	local length = string.len(message);

	while (startPos <= length) do
		endPos = length;
		local HPos = string.find (message, "|H", startPos, true);
		if (HPos) then
			endPos = HPos;
		end

		msg = msg .. addEmoticon (string.sub (message, startPos, endPos), style);

		startPos = endPos + 1;
		if (HPos) then
			local hPos = string.find (message, "|h", startPos, true);
			if (hPos) then
				endPos = hPos;
			else
				endPos = length;
			end

			msg = msg .. string.sub (message, startPos, endPos);
			startPos = endPos + 1;
		end
	end
	return msg;
end

--- API for external addons to get installed style
function lib:GetStyles ()
	return tabStili;
end

function lib:HasStyles ()
	local count = 0;
	for k, v in pairs (tabStili) do
		count = count + 1;
	end
	return count > 0;
end