-- Author      : Galilman
-- Create Date : 20/10/2014 17:42:00

local lib = LibStub:NewLibrary("gxchat-LibComunicazione", 2)

if (not lib) then
	return;
end

lib.callbacks = lib.callbacks or LibStub("CallbackHandler-1.0"):New(lib, "RegisterMessage", "UnregisterMessage", false);

local CTL = assert(ChatThrottleLib, "gxchat-LibComunicazione requires ChatThrottleLib");

local libS = LibStub:GetLibrary ("AceSerializer-3.0");
local libC = LibStub:GetLibrary ("LibCompress");
local libCE = libC:GetChatEncodeTable ();

lib.idMessaggio = lib.idMessaggio or 0;
local idPlayer = UnitGUID ("player");
local nomeServer = GetRealmName ();
local playerFaction = UnitFactionGroup("player");
local channelIndex;
local playerChannelName;

lib.tabMessaggi = lib.tabMessaggi or {};
lib.messaggiInviati = lib.messaggiInviati or {};
lib.messaggiInviatiCount = lib.messaggiInviatiCount or 0;

local PREFIX = "gxchat_LibCom";

local MAX_LUNGHEZZA_PACCHETTO = 253;
local MAX_LUNGHEZZA_PACCHETTO_BNET = 4076;
local PACCHETTO_SINGOLO = "\001";
local INIZIO_PACCHETTO_MULTIPLO = "\002";
local CONTINUAZIONE_PACCHETTO_MULTIPLO = "\003";
local FINE_PACCHETTO_MULTIPLO = "\004";

lib.tabellaParti = lib.tabellaParti or {};

local MAX_TIME = 1000 * 60 * 2;
local timer_bnet = 0.5;
local timer_messaggi = 120;

local trascorso_messaggi = 0;
local trascorso_bnet = 0;

--Invio messaggio
local function InviaMessaggio (tipo, messaggio, dest, canale, ...)
	local pacchettoReinoltro = ...;
	local pacchetto = pacchettoReinoltro or {};
	if (not pacchettoReinoltro) then
		pacchetto.id = idPlayer .. ":" .. lib.idMessaggio;
		lib.idMessaggio = lib.idMessaggio + 1;
		pacchetto.tipo = tipo;
		pacchetto.destinazione = dest;

		if (messaggio) then
			if (type (messaggio) == "table") then
				pacchetto.messaggio = libS:Serialize (messaggio);
			else
				--pacchetto.messaggio = messaggio;
				error ("message: table expected, got " .. type (messaggio));
			end
		else
			error ("message: table expected, got " .. nil);
		end
	end

	local ser = libS:Serialize (pacchetto);
	local compr = libC:Compress (ser);
	local msg = libCE:Encode (compr);

	if (not canale or canale == "local") then
		if (#msg < MAX_LUNGHEZZA_PACCHETTO) then
			msg = PACCHETTO_SINGOLO .. msg;

			CTL:SendAddonMessage ("NORMAL", PREFIX, msg, "CHANNEL", channelIndex);
		else
			local parte = string.sub (msg, 1, MAX_LUNGHEZZA_PACCHETTO);
			CTL:SendAddonMessage ("NORMAL", PREFIX, INIZIO_PACCHETTO_MULTIPLO .. parte, "CHANNEL", channelIndex);
			local pos = 1 + MAX_LUNGHEZZA_PACCHETTO;
			local numParti = 1;
			while (pos + MAX_LUNGHEZZA_PACCHETTO <= #msg) do
				numParti = numParti + 1;
				parte = string.sub (msg, pos, pos + MAX_LUNGHEZZA_PACCHETTO - 1);
				CTL:SendAddonMessage ("NORMAL", PREFIX, CONTINUAZIONE_PACCHETTO_MULTIPLO .. parte, "CHANNEL", channelIndex);
				pos = pos + MAX_LUNGHEZZA_PACCHETTO;
			end
			--Ultimo pacchetto
			--Comunicazione:Print ("messaggio multiplo, ultima parte");
			parte = string.sub (msg, pos);
			CTL:SendAddonMessage ("NORMAL", PREFIX, FINE_PACCHETTO_MULTIPLO .. parte, "CHANNEL", channelIndex);
		end
	end

	if (not canale or canale == "battle.net") then
		if (BNConnected ()) then
			lib.messaggiInviati[pacchetto.id] = {};
			lib.messaggiInviati[pacchetto.id].amici = {};
			lib.messaggiInviati[pacchetto.id].server = {};
			lib.messaggiInviati[pacchetto.id].server[nomeServer] = true;
				--Suddivisione in messaggi e invio
			if (#msg < MAX_LUNGHEZZA_PACCHETTO_BNET) then
				msg = PACCHETTO_SINGOLO .. msg;
				lib.messaggiInviati[pacchetto.id].messaggio = msg;
			else
				--Comunicazione:Print ("messaggio multiplo");
				local parte = string.sub (msg, 1, MAX_LUNGHEZZA_PACCHETTO_BNET);
				local pos = 1 + MAX_LUNGHEZZA_PACCHETTO_BNET;
				local numParti = 1;
				lib.messaggiInviati[pacchetto.id].messaggio = {};
				lib.messaggiInviati[pacchetto.id].messaggio[numParti] = INIZIO_PACCHETTO_MULTIPLO .. parte;
				while (pos + MAX_LUNGHEZZA_PACCHETTO_BNET <= #msg) do
					numParti = numParti + 1;
					parte = string.sub (msg, pos, pos + MAX_LUNGHEZZA_PACCHETTO_BNET - 1);
					lib.messaggiInviati[pacchetto.id].messaggio[numParti] = CONTINUAZIONE_PACCHETTO_MULTIPLO .. parte;
					pos = pos + MAX_LUNGHEZZA_PACCHETTO_BNET;
				end
				--Ultimo pacchetto
				parte = string.sub (msg, pos);
				lib.messaggiInviati[pacchetto.id].messaggio[numParti + 1] = FINE_PACCHETTO_MULTIPLO .. parte;
			end

			lib.messaggiInviatiCount = lib.messaggiInviatiCount + 1;
		end
	end

	lib.tabMessaggi [pacchetto.id] = GetTime ();
end

function lib:BNET_ACK (msg)
	if (lib.messaggiInviati[msg.idMessaggio]) then
		lib.messaggiInviati[msg.idMessaggio].server[msg.server] = true;
	end
end

local function pacchettoCompletato (pacchetto, canale, sender)
	local decod = libCE:Decode (pacchetto);
	local decompr, err = libC:Decompress (decod);
	if (not decompr) then
		return;
	end

	local successo, pkt = libS:Deserialize (decompr);
	if (not successo) then
		return;
	end

	local presente = true;
	--A Questo punto il pacchetto � leggibile
	if (not lib.tabMessaggi[pkt.id]) then
		--Messaggio mai arrivato, lo elaboro
		local messaggio = pkt.messaggio;
		successo, messaggio = libS:Deserialize (messaggio);
		if (not successo) then
			return;
		end
		if (pkt.tipo == "BNET_ACK") then
			lib:BNET_ACK (messaggio);
		else
			lib.callbacks:Fire (pkt.tipo, messaggio, canale);
		end
		presente = false;
	end
	
	lib.tabMessaggi[pkt.id] = GetTime ();

	--Devo mandare conferma ricezione
	if (pkt.tipo ~= "BNET_ACK" and canale == "battle.net") then
		local pacchetto = {};
		pacchetto.tipo = "BNET_ACK";
		pacchetto.id = idPlayer .. ":" .. lib.idMessaggio;
		lib.idMessaggio = lib.idMessaggio + 1;
		
		local msg = {};
		msg.idMessaggio = pkt.id;
		msg.server = nomeServer;
		pacchetto.messaggio = libS:Serialize (msg);

		local ser = libS:Serialize (pacchetto);
		local compr = libC:Compress (ser);
		local messaggio = libCE:Encode (compr);

		BNSendGameData (sender, PREFIX, PACCHETTO_SINGOLO .. messaggio);
	end

	--Reinoltro
	if (presente) then
		return;
	elseif (pkt.tipo ~= "BNET_ACK" and canale == "battle.net") then
		InviaMessaggio (nil, nil, nil, "local", pkt);
	elseif (pkt.destinazione == "battle.net") then
		InviaMessaggio (nil, nil, nil, "battle.net", pkt);
	end
end

local function Messaggio_INIZIO_MESSAGGIO_MULTIPLO (messaggio, sender, canale)
	local chiave = sender .. "\t" .. canale;

	lib.tabellaParti[chiave] = messaggio;
end

local function Messaggio_CONTINUAZIONE_MESSAGGIO (messaggio, sender, canale)
	local chiave = sender .. "\t" .. canale;
	local datiInseriti = lib.tabellaParti[chiave];

	if (not datiInseriti) then
		--Mancano i pezzi precedenti
		return;
	end

	if (type (datiInseriti) ~="table") then
		local t = {};
		t[1] = datiInseriti;
		t[2] = messaggio;
		lib.tabellaParti[chiave] = t;
	else
		table.insert (datiInseriti, messaggio);
	end
end

local function Messaggio_FINE_MESSAGGIO_MULTIPLO (messaggio, sender, canale)
	local chiave = sender .. "\t" .. canale;
	local datiInseriti = lib.tabellaParti[chiave];

	if (not datiInseriti) then
		--Mancano i pezzi precedenti
		return;
	end

	lib.tabellaParti[chiave] = nil;
	if (type (datiInseriti) == "table") then
		table.insert(datiInseriti, messaggio);
		local pacchetto = table.concat (datiInseriti);
		pacchettoCompletato (pacchetto, canale, sender);
	else
		pacchettoCompletato (datiInseriti .. messaggio, canale, sender);
	end
end

local function processaPacchetto (message, sender, canale)
	local control = string.sub (message, 1);
	if (control) then
		if (string.byte (control) == string.byte (PACCHETTO_SINGOLO)) then
			pacchettoCompletato (string.sub (message, 2), canale, sender);
		elseif (string.byte (control) == string.byte(INIZIO_PACCHETTO_MULTIPLO)) then
			Messaggio_INIZIO_MESSAGGIO_MULTIPLO (string.sub (message, 2), sender, canale);
		elseif (string.byte (control) == string.byte (CONTINUAZIONE_PACCHETTO_MULTIPLO)) then
			Messaggio_CONTINUAZIONE_MESSAGGIO (string.sub (message, 2), sender, canale);
		elseif (string.byte (control) == string.byte (FINE_PACCHETTO_MULTIPLO)) then
			Messaggio_FINE_MESSAGGIO_MULTIPLO (string.sub (message, 2), sender, canale);
		end
	end
end

local function CHANNEL_UI_UPDATE (...)
	lib.frame:UnregisterEvent ("CHANNEL_UI_UPDATE")
	JoinTemporaryChannel ("gxchatLibComunicazione")

	channelIndex = GetChannelName ("gxchatLibComunicazione");
end

local function PLAYER_ENTERING_WORLD (...)
	if (not idPlayer) then
		idPlayer = UnitGUID ("player");
	end
	if (not nomeServer) then
		nomeServer = GetRealmName ();
	end
	if (not playerFaction) then
		playerFaction = UnitFactionGroup("player");
	end

	lib.frame:UnregisterEvent ("PLAYER_ENTERING_WORLD");

	if GetNumDisplayChannels() > 0 then 
		-- Se si aggiungo il mio
		CHANNEL_UI_UPDATE()
	else
		lib.frame:RegisterEvent ("CHANNEL_UI_UPDATE")
	end

	local a,b = UnitFullName ("player");
	playerChannelName = a .. "-" .. b;

	RegisterAddonMessagePrefix (PREFIX);
end

local function OnEvent (self, event, ...)
	if (event == "CHAT_MSG_ADDON") then
		local prefix, message, distribution, sender = ...;
		if (prefix == PREFIX and sender ~= playerChannelName) then
			processaPacchetto (message, sender, "local");
		end
	elseif (event == "BN_CHAT_MSG_ADDON") then
		local prefix, message, tipoMessaggio, senderToonID = ...;
		if (prefix == PREFIX) then
			processaPacchetto (message, senderToonID, "battle.net");
		end
	elseif (event == "PLAYER_ENTERING_WORLD") then
		PLAYER_ENTERING_WORLD (...);
	elseif (event == "CHANNEL_UI_UPDATE") then
		CHANNEL_UI_UPDATE (...);
	end
end

local function OnUpdate (self, elapsed)
	trascorso_bnet = trascorso_bnet + elapsed;
	trascorso_messaggi = trascorso_messaggi + elapsed;

	-- Invio su bnet
	if (trascorso_bnet > timer_bnet and lib.messaggiInviatiCount > 0) then
		totali, online = BNGetNumFriends ();
		for i = 1, online do
			local presenceID, presenceName, battleTag, isBattleTagPresence, toonName, toonID, client, isOnline, lastOnline, isAFK, isDND, broadcastText, noteText, isRIDFriend, broadcastTime, canSoR  = BNGetFriendInfo(i);
			if (isOnline) then
				local nToons = BNGetNumFriendGameAccounts(i)
				local unknown, toonName, client, realmName, realmID, faction, race, class, unknown1, zoneName, level, gameText, broadcastText, broadcastTime, unknown2, presenceID1;
				if (nToons > 0) then
					for index = 1, nToons do
						unknown, toonName, client, realmName, realmID, faction, race, class, unknown1, zoneName, level, gameText, broadcastText, broadcastTime, unknown2, presenceID1 = BNGetFriendGameAccountInfo(i, index);
						if (client == "WoW") then
							break;
						end
					end
				else
					unknown, toonName, client, realmName, realmID, faction, race, class, unknown1, zoneName, level, gameText, broadcastText, broadcastTime, unknown2, presenceID1 = BNGetGameAccountInfo(presenceID);
				end
				for k, v in pairs (lib.messaggiInviati) do
					if (not v.inviato and playerFaction == faction and not v.amici[presenceID] and not v.server[realmName]) then
						if (type (v.messaggio) == "string") then
							BNSendGameData (presenceID1, PREFIX, v.messaggio);
						else
							for i = 1, #v.messaggio do
								BNSendGameData (presenceID1, PREFIX, v.messaggio[i]);
							end
						end
						v.inviato = true;
						v.amici[presenceID] = true;
					end
				end
			end
		end

		--Se non ha inviato a nessuno vuol dire che tutti gli amici connessi hanno gi� ricevuto il pacchetto, quindi viene rimosso;
		--Se � stato inviato viene segnato da inviare per il prossimo "giro"
		local num = 0;
		for k, v in pairs (lib.messaggiInviati) do
			if (not v.inviato) then
				lib.messaggiInviati[k] = nil;
				lib.messaggiInviatiCount = lib.messaggiInviatiCount - 1;
			else
				v.inviato = false;
				num = num + 1;
			end
		end
		trascorso_bnet = 0;
	end

	-- Pulizia tabella messaggi arrivati
	if (trascorso_messaggi > timer_messaggi) then
		trascorso_messaggi = 0;
		local tempoCorrente = GetTime ();
		for k, v in pairs (lib.tabMessaggi) do
			if ((tempoCorrente - v) > MAX_TIME) then
				lib.tabMessaggi[k] = nil;
			end
		end
	end
end

lib.frame = lib.frame or CreateFrame ("Frame", "gxchat_LibComunicazione_frame");
lib.frame:SetScript ("OnEvent", OnEvent);
lib.frame:SetScript ("OnUpdate", OnUpdate);
lib.frame:UnregisterAllEvents ();
lib.frame:RegisterEvent ("CHAT_MSG_ADDON");
lib.frame:RegisterEvent ("BN_CHAT_MSG_ADDON");
lib.frame:RegisterEvent ("PLAYER_ENTERING_WORLD");

--- Send a message in local channel
-- @param type message type (used for callback on receive)
-- @param message message to send
function lib:SendLocalMessage (type, message)
	InviaMessaggio (type, message, "local", "local");
end

--- Send a message in local channel and in battle.net network
-- @param type message type (used for callback on receive)
-- @param message message to send
function lib:SendMessage (type, message)
	InviaMessaggio (type, message, "battle.net", nil);
end